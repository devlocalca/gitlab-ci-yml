FROM python:3.9.7-alpine
RUN apk add bash vim
SHELL ["/bin/bash", "-c"]
RUN apk update
RUN apk upgrade

# upgrade pip, install node, install cdk, add user/group 'cdk'
RUN python -m pip install --upgrade pip
RUN apk add --update nodejs npm
RUN npm install -g aws-cdk

RUN addgroup -S caleb 
RUN adduser -s /bin/bash -S -G caleb caleb 
WORKDIR /home/caleb
# RUN python -m pip install -r requirements.txt
USER caleb

# ------------------------------------------------------------------------------
# copy repo files into container (this only needed when running on local dev machine)
#ADD app.py cdk.json requirements.txt setup.py README.md .
#RUN mkdir pdl_storagezone_app
#WORKDIR /home/cdk/pdl_storagezone_app
#ADD pdl_storagezone_app .

# ------------------------------------------------------------------------------
#USER root
#WORKDIR /home/cdk
#RUN python -m pip install -r requirements.txt
#USER cdk
#WORKDIR /home/cdk

# ------------------------------------------------------------------------------
#run deploy/destroy
#RUN cdk deploy --require-approval never
#RUN cdk destroy --force
#ENTRYPOINT ["cdk", "deploy", "--require-approval", "never"]
# last run: 2021-09-08 0